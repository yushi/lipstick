function RGB2HSV (r, g, b) {
    var chigh = Math.max(r, g, b);
    var clow = Math.min(r, g, b);
    var crange = chigh - clow;
    var s = (chigh>0) ? (crange/chigh) : 0;
    var v = chigh / 255;

    var h;
    
    if (r===g && g===b) {
        h = 0; // actually undefined, but need a value
    } else {
        var rp = (chigh-r)/crange;
        var gp = (chigh-g)/crange;
        var bp = (chigh-b)/crange;

        var hp;
        if (r===chigh) {
            hp = bp - gp;
        } else if (g===chigh) {
            hp = rp - bp + 2;
        } else { //b===chigh 
            hp = gp - rp + 4;
        }
        if (hp < 0) {
            h = hp/6+1;
        } else {
            h = hp/6;
        }
    }

    return [h, s, v];
}

function HSV2RGB (h, s, v) {
    var hp = (6 * h) % 6;
    var c1 = Math.floor(hp);
    var c2 = hp - c1;

    var x = (1 - s)*v;
    var y = (1 - s*c2)*v;
    var z = (1 - s*(1-c2))*v;
    
    var rpgpbp = [
        [v, z, x],
        [y, v, x],
        [x, v, z],
        [x, y, v],
        [z, x, v],
        [v, x, y]
    ][c1];

    return rpgpbp.map(function (n) {
        return Math.min(Math.round(256*n), 255);
    });
}


var HIGHLIGHT_COLOR = '#f0f0f0';
var AUX_LINE_COLOR = 0xdddddd;

function makeLine (x1, y1, z1, x2, y2, z2, colorOrDashParams) {
    var material;
    if (!!colorOrDashParams.dashSize) {
        material = new THREE.LineDashedMaterial(colorOrDashParams);
    } else {
        material = new THREE.LineBasicMaterial({color: colorOrDashParams});
    }
    var geometry = new THREE.Geometry();

    geometry.vertices.push(new THREE.Vector3(x1, y1, z1));
    geometry.vertices.push(new THREE.Vector3(x2, y2, z2));
    return line = new THREE.Line(geometry, material);
}

function setLineEnds (line, x1, y1, z1, x2, y2, z2) {
    line.geometry.vertices = [new THREE.Vector3(x1, y1, z1),
                              new THREE.Vector3(x2, y2, z2)];
    line.geometry.verticesNeedUpdate = true;
    line.geometry.computeLineDistances() // required for dashed otherwise shows solid
}

// must call camera.updateMatrixWorld prior to invocation
function addText (text, x, y, z, color, camera, backgroundContext) {
    var p = new THREE.Vector3(x, y, z);
    p.project(camera);
    
    var xx = (p.x+1) * backgroundContext.canvas.width /2;
    var yy = (1-p.y) * backgroundContext.canvas.height /2;
    backgroundContext.fillStyle = color;
    backgroundContext.fillText(text, xx,  yy);
}

function useBackgroundCanvas  (mainCanvas, scene, color) {
    var backgroundCanvas = document.createElement('canvas');
    backgroundCanvas.width = mainCanvas.width;
    backgroundCanvas.height = mainCanvas.height;
    backgroundContext = backgroundCanvas.getContext('2d');
    backgroundContext.fillStyle = color;
    backgroundContext.fillRect(0, 0, backgroundCanvas.width, backgroundCanvas.height);
    backgroundContext.font = '20px sans-serif';
    
    var backgroundTexture = new THREE.CanvasTexture(backgroundContext.canvas);
    scene.background = backgroundTexture;

    return backgroundContext
}


$(function () {
    var highlight = 'l';
    var dirty;
    var currentHSV;
    var currentRGB;

    function setHSV (h, s, v) {
        var rgb = HSV2RGB(h, s, v);
        setColor(h, s, v, rgb[0], rgb[1], rgb[2]);
    }

    function setRGB (r, g, b) {
        var hsv = RGB2HSV(r, g, b);
        setColor(hsv[0], hsv[1], hsv[2], r, g, b);
    }

    function setColor (h, s, v, r, g, b) {
        currentHSV = [h, s, v];
        currentRGB = [r, g, b];
        $('#colorR').text(r);
        $('#colorG').text(g);
        $('#colorB').text(b);
        $('#colorH').text(h);
        $('#colorS').text(s);
        $('#colorV').text(v);
    }

    function update (dirtyStuff) {
        dirty = dirtyStuff;
        requestAnimationFrame(render);
    }

    $('#webglcanvas').mousemove(function (event) {
        var parentOffset = $(this).parent().offset(); 
        var x = event.pageX - parentOffset.left;
        //var y = event.pageY - parentOffset.top;
        var shouldHighlight = (x<$('#webglcanvas').width()/2)?'l':'r';
        if (highlight !== shouldHighlight) {
            highlight = shouldHighlight;
            update('highlight');
        }
    });
    
    var canvas = document.getElementById('webglcanvas');
    var renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});
    renderer.setSize(canvas.width, canvas.height);
    renderer.setClearColor(0xffffff, 1);
    renderer.autoClear = false;
    renderer.clear();


    
    var renderRGB = (function () {
        var scene, camera;
        var projLines = new Array(9);

        function renderBackground () {
            var backgroundContext = useBackgroundCanvas(canvas, scene, highlight==='r'?HIGHLIGHT_COLOR:'White');
            camera.updateMatrixWorld(); // so .project() can work
            addText('R', 260, 0, 0, 'red', camera, backgroundContext);
            addText('G', 0, 260, 0, 'green', camera, backgroundContext);
            addText('B', 0, 0, 260, 'blue', camera, backgroundContext);
        }

        var sphere;

        return function () {
            
            if (!scene) { // full re-render
                scene = new THREE.Scene();

                camera = new THREE.OrthographicCamera(-300, 300, 300, -300, 0, 10000 );
                camera.position.set(400, 400, 800); 
                camera.lookAt(new THREE.Vector3(0, 0, 0));


                var group = new THREE.Group;

                var material = new THREE.LineBasicMaterial({ color: 0 });
                [[255, 0, 0], [0, 255, 0], [0, 0, 255]].forEach(function (l) {
                    var geometry = new THREE.Geometry(); 
                    geometry.vertices.push(new THREE.Vector3(0, 0, 0)); 
                    geometry.vertices.push(new THREE.Vector3(l[0], l[1], l[2])); 
                    var line = new THREE.Line(geometry, material);
                    group.add(line);
                });

                scene.add(group);

                var geometry = new THREE.SphereBufferGeometry(12, 32, 32 ); 
                var material = new THREE.MeshBasicMaterial({color: 'rgb('+currentRGB.join(',')+')'} ); 
                sphere = new THREE.Mesh( geometry, material ); 
                sphere.position.set(currentRGB[0], currentRGB[1], currentRGB[2]);

                scene.add(sphere);

                renderBackground();

                for (var i=0; i<9; i++) {
                    projLines[i] = makeLine(0, 0, 0, 0, 0, 0, AUX_LINE_COLOR);
                    group.add(projLines[i]);
                }
            } else if (dirty === 'highlight') {
                renderBackground();
            } else if (dirty === 'cursor') {
                sphere.position.set(currentRGB[0], currentRGB[1], currentRGB[2]);
                sphere.material.color = new THREE.Color('rgb('+currentRGB.join(',')+')');
            }
            setLineEnds(projLines[0], currentRGB[0], currentRGB[1], currentRGB[2],
                        0, currentRGB[1], currentRGB[2]);
            setLineEnds(projLines[1], currentRGB[0], currentRGB[1], currentRGB[2],
                        currentRGB[0], 0, currentRGB[2]);
            setLineEnds(projLines[2], currentRGB[0], currentRGB[1], currentRGB[2],
                        currentRGB[0], currentRGB[1], 0);
            setLineEnds(projLines[3], 0, currentRGB[1], currentRGB[2],
                        0, currentRGB[1], 0);
            setLineEnds(projLines[4], currentRGB[0], currentRGB[1], 0,
                        0, currentRGB[1], 0);
            setLineEnds(projLines[5], 0, currentRGB[1], currentRGB[2],
                        0, 0, currentRGB[2]);
            setLineEnds(projLines[6], currentRGB[0], currentRGB[1], 0,
                        currentRGB[0], 0, 0);
            setLineEnds(projLines[7], currentRGB[0], 0, currentRGB[2],
                        0, 0, currentRGB[2]);
            setLineEnds(projLines[8], currentRGB[0], 0, currentRGB[2],
                        currentRGB[0], 0, 0);

            renderer.setViewport(canvas.width/2, 0, canvas.width/2, canvas.height);
            renderer.render(scene, camera);
        };
    })();

    var renderHSV = (function () {
        var RADIUS = 130;
        var HEIGHT = 260;

        var scene;
        

        function renderBackground () {
            var bgCtx = useBackgroundCanvas(canvas, scene, highlight==='l'?HIGHLIGHT_COLOR:'White');
            camera.updateMatrixWorld();
            addText('S', 200, 0, 0, 'black', camera, bgCtx);
            addText('V', 0, 360, 0, 'black', camera, bgCtx);
        }

        var sphere;
        var group;
        var projToV, projToBottom, fan;

        function makeFan () {
            var geometry = new THREE.CircleGeometry(currentHSV[1]*RADIUS,
                                                    Math.round(currentHSV[0]*128),
                                                    0,
                                                    currentHSV[0]*2*Math.PI);
            var material = new THREE.MeshBasicMaterial({color: 0xdddddd});
            material.side = THREE.DoubleSide;
            var fan = new THREE.Mesh(geometry, material);
            fan.rotateX(Math.PI/2);
            return fan;
        }

        return function () {
            var pos = [currentHSV[1]*RADIUS*Math.cos(Math.PI*2*currentHSV[0]), // x/s
                       currentHSV[2]*HEIGHT, // v/y
                       currentHSV[1]*RADIUS*Math.sin(Math.PI*2*currentHSV[0])]; //z


            if (!scene) {
                scene = new THREE.Scene();

                camera = new THREE.OrthographicCamera(-300, 300, 400, -200, 0, 10000 );
                camera.position.set(0, 100, 300); 
                camera.lookAt(new THREE.Vector3(0, 0, 0));

                group = new THREE.Group;

                var material = new THREE.LineBasicMaterial({ color: 0 });

                var curve = new THREE.EllipseCurve( 
                    0, 0, // ax, aY 
                    RADIUS, RADIUS, // xRadius, yRadius 
                    0, 2 * Math.PI, // aStartAngle, aEndAngle 
                    false, // aClockwise 
                    0 // aRotation 
                ); 
                var path = new THREE.Path( curve.getPoints( 64 ) ); 
                var geometry = path.createPointsGeometry( 64 );
                
                var circle = new THREE.Line(geometry, material);

                circle.rotateX(Math.PI/2);
                group.add(circle);

                circle = circle.clone();
                circle.position.set(0, HEIGHT, 0);
                group.add(circle);

                // V axis
                group.add(makeLine(0, 0, 0, 0, 360, 0, 0));
                // S axis
                group.add(makeLine(0, 0, 0, 200, 0, 0, 0));
                // left & right border
                group.add(makeLine(RADIUS, 0, 0, RADIUS, 255, 0, 0));
                group.add(makeLine(-RADIUS, 0, 0, -RADIUS, 255, 0, 0));
                scene.add(group);

                renderBackground();

                var geometry = new THREE.SphereBufferGeometry(12, 32, 32 ); 
                var material = new THREE.MeshBasicMaterial({color: 'rgb('+currentRGB.join(',')+')'} ); 
                sphere = new THREE.Mesh( geometry, material ); 
                sphere.position.set(pos[0], pos[1], pos[2]);
                group.add(sphere);

                projToV = makeLine(0, 0, 0, 0, 0, 0, AUX_LINE_COLOR);

                projToBottom = makeLine(0, 0, 0, 0, 0, 0, AUX_LINE_COLOR);
                group.add(projToV);
                group.add(projToBottom);
                
                fan = makeFan();
                group.add(fan);

            } else if (dirty==='highlight') {
                renderBackground();
            } else if (dirty==='cursor') {
                sphere.material.color = new THREE.Color('rgb('+currentRGB.join(',')+')');
                sphere.position.set(pos[0], pos[1], pos[2]);

                group.remove(fan);
                fan = makeFan();
                group.add(fan);
            }
            
            setLineEnds(projToBottom, pos[0], pos[1], pos[2],
                        pos[0], 0, pos[2]);
            setLineEnds(projToV, pos[0], pos[1], pos[2], 0, pos[1], 0);

            renderer.setViewport(0, 0, canvas.width/2, canvas.height);
            renderer.render(scene, camera);

        };
    })();

    function render () {
        renderRGB();
        renderHSV();
    }

    setRGB(230, 40, 120);
    render();

    var speed = 1;

    $(document).keypress(function (ev) {
        var ch = String.fromCharCode(ev.which);
        var spec = {
            R: [currentRGB, 0,  1, 0, 256],
            r: [currentRGB, 0, -1, 0, 256],
            G: [currentRGB, 1,  1, 0, 256],
            g: [currentRGB, 1, -1, 0, 256],
            B: [currentRGB, 2,  1, 0, 256],
            b: [currentRGB, 2, -1, 0, 256],

            H: [currentHSV, 0,  0.01, 0, 1],
            h: [currentHSV, 0, -0.01, 0, 1],
            S: [currentHSV, 1,  0.01, 0, 1],
            s: [currentHSV, 1, -0.01, 0, 1],
            V: [currentHSV, 2,  0.01, 0, 1],
            v: [currentHSV, 2, -0.01, 0, 1]
        }[ch];

        if (!spec) {
            return;
        }

        var newVal = Math.round(spec[0][spec[1]]*100 + speed*spec[2]*100)/100;
        if (newVal<spec[3] || newVal>spec[4]) {
            // limit values but let hue wrap
            if (ch==='H') {
                newVal = 0;
            } else if (ch==='h') {
                newVal = 1;
            } else {
                return;
            }
        }

        spec[0][spec[1]] = newVal;

        ((spec[0]===currentRGB)?setRGB:setHSV).apply(null, spec[0]);

        update('cursor');
    }); // keypress

    


var LIPSTICKS = [
    //https://www.maybelline.com/lip-makeup/lipstick/color-sensational-lip-color/pink-peony
    {name: 'Maybelline Color Sensational "Pink Peony"',
     rgb: 'f27077'},
    {name: 'Maybelline Color Sensational "Pink & Proper"',
     rgb: 'fa6c98'},
    {name: 'Maybelline Color Sensational "Sugar Chic"',
     rgb: 'd77498'},
    {name: 'Maybelline Color Sensational "Born With It"',
     rgb: 'df8a82'},
    {name: 'Maybelline Color Sensational "Pink Sand"',
     rgb: 'fbcfcb'},
    {name: 'Maybelline Color Sensational "Crazy for Coffee"',
     rgb: '8f332c'}
];

function parseRGB (s) {
    return [0, 2, 4].map(function (i) {
        return parseInt(s.substr(i, 2), 16);
    });
}

function round (v) {
    return Math.round(v*1000)/1000;
}

var NAME1 = $('#name1');
var NAME2 = $('#name2');
var COLOR1 = $('#color1');
var COLOR2 = $('#color2');
var SUBMIT_BTN = $('#submit');
var HUE1 = $('#hue1');
var HUE2 = $('#hue2');
var SAT1 = $('#sat1');
var SAT2 = $('#sat2');
var VAL1 = $('#val1');
var VAL2 = $('#val2');
var ANSWERH = $('#answerH');
var ANSWERS = $('#answerS');
var ANSWERV = $('#answerV');
var NOTICE = $('#clickNotice');

var i1, i2;
function startQuiz () {
    SUBMIT_BTN.text('Submit');
    i1 = Math.floor(Math.random()*LIPSTICKS.length);
    i2 = (Math.floor(Math.random()*(LIPSTICKS.length-1))+i1+1)%LIPSTICKS.length;
    NAME1.text(LIPSTICKS[i1].name);
    NAME2.text(LIPSTICKS[i2].name);
    COLOR1.css('background-color', '#'+LIPSTICKS[i1].rgb);
    COLOR2.css('background-color', '#'+LIPSTICKS[i2].rgb);

    [HUE1, HUE2, SAT1, SAT2, VAL1, VAL2].forEach(function (e) {
        e.css('visibility', 'hidden');
    });
    [ANSWERH, ANSWERS, ANSWERV].forEach(function (e) {
        e.css('background-color', '#fff');
    });
    NOTICE.css('visibility', 'hidden');
    COLOR1.off('click');
    COLOR2.off('click');
    SUBMIT_BTN.one('click', submit);
}

function submit () {
    SUBMIT_BTN.text('Next');
    var rgb1 = parseRGB(LIPSTICKS[i1].rgb);
    var rgb2 = parseRGB(LIPSTICKS[i2].rgb);
    var hsv1 = RGB2HSV.apply(null, rgb1).map(round);
    var hsv2 = RGB2HSV.apply(null, rgb2).map(round);

    var hue1, hue2;
    if (hsv1[0]>0.5) {
        hue1 = ''+hsv1[0]+' = 1 - '+round(1-hsv1[0]);
    } else {
        hue1 = hsv1[0];
    }

    if (hsv2[0]>0.5) {
        hue2 = ''+hsv2[0]+' = 1 - '+round(1-hsv2[0]);
    } else {
        hue2 = hsv2[0];
    }

    HUE1.text(hue1); 
    HUE2.text(hue2);
    SAT1.text(hsv1[1]);
    SAT2.text(hsv2[1]);
    VAL1.text(hsv1[2]);
    VAL2.text(hsv2[2]);
    [HUE1, HUE2, SAT1, SAT2, VAL1, VAL2].forEach(function (e) {
        e.css('visibility', 'visible');
    });

    [ANSWERH, ANSWERS, ANSWERV].map(function (e, i) {
        var s1 = e.find('input')[0].checked;
        var correct;

        if (i===0) {
            var red1 = Math.min(hsv1[0], 1-hsv1[0]);
            var red2 = Math.min(hsv2[0], 1-hsv2[0]);
            correct = (red1===red2) ||
                (red1<red2 && s1) ||
                (red1>red2 && !s1);
        } else {
            correct = (hsv1[i]===hsv2[i]) ||
            (hsv1[i]>hsv2[i] && s1) ||
            (hsv1[i]<hsv2[i] && !s1);
        }
        e.css('background-color', correct ? 'green' : 'red');
       
    });

    NOTICE.css('visibility', 'visible');
    COLOR1.click(function () {
        setHSV.apply(null, hsv1);
        update('cursor');
    });
    COLOR2.click(function () {
        setHSV.apply(null, hsv2);
        update('cursor');
    });
    SUBMIT_BTN.one('click', startQuiz);
}

startQuiz();
}); // $
